package com.customid.shinta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimulationAndTechnicalTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimulationAndTechnicalTaskApplication.class, args);
	}

}
