package com.customid.shinta.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.customid.shinta.config.DefaultResponse;
import com.customid.shinta.model.dto.ImageDto;
import com.customid.shinta.model.entity.ImageEntity;
import com.customid.shinta.repository.ImageRepository;


@RestController
@RequestMapping ("/image")
public class ImageController {
	@Autowired 
	private ImageRepository repository;
	
	
	@GetMapping
	public List<ImageDto> get() {
		List<ImageEntity> list = repository.findAll();
		List<ImageDto> dtoList = list.stream().map(this::convertToDto)
				.collect(Collectors.toList());
		return dtoList;
	}
	
	@GetMapping ("/{id}")
	public DefaultResponse getById(@PathVariable String id) {
		if(repository.findById(id).isPresent() == false) {
			return DefaultResponse.ok("Data Tidak Ditemukan");
		}
		ImageEntity entity = repository.findById(id).get();
		ImageDto dto = convertToDto(entity);
		return DefaultResponse.ok(dto);
	}
	
	@PostMapping
	public DefaultResponse insert(@RequestBody ImageDto dto) {
		ImageEntity entity = convertToEntity(dto);
		repository.save(entity);
		return DefaultResponse.ok("Data Berhasil Di Input");
	}
	
	@PutMapping ("/update/{id}")
	public DefaultResponse update(@RequestBody ImageDto dto, @PathVariable String id) {
		if(repository.findById(id).isPresent() == false) {
			return DefaultResponse.ok("Data Tidak Ditemukan");
		}
		ImageEntity entity = convertToEntity(dto);
		entity.setIdReq(id);
		repository.save(entity);
		return DefaultResponse.ok("Data Berhasil di Update");
	}
	
	@DeleteMapping ("/delete/{id}") 
	public DefaultResponse delete(@PathVariable String id) {
		if(repository.findById(id).isPresent() == false) {
			return DefaultResponse.ok("Data Tidak Ditemukan");
		}
		repository.deleteById(id); 
		return DefaultResponse.ok("Data Berhasil di Hapus");
	}
	
	private ImageDto convertToDto(ImageEntity entity) {
		ImageDto dto = new ImageDto();
        dto.setIdReq(entity.getIdReq());
        dto.setDescription(entity.getDescription());
		return dto;
	}
	private ImageEntity convertToEntity(ImageDto dto) {
        ImageEntity entity = new ImageEntity();
        entity.setIdReq(dto.getIdReq());
        entity.setDescription(dto.getDescription());
		return entity;
	}
}
