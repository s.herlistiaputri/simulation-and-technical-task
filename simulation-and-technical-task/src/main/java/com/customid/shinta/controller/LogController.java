package com.customid.shinta.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.customid.shinta.config.DefaultResponse;
import com.customid.shinta.model.dto.LogDto;
import com.customid.shinta.model.entity.LogEntity;
import com.customid.shinta.repository.LogRepository;

@RestController
@RequestMapping ("/log")
public class LogController {
	@Autowired 
	private LogRepository repository;
	
	
	@GetMapping
	public List<LogDto> get() {
		List<LogEntity> list = repository.findAll();
		List<LogDto> dtoList = list.stream().map(this::convertToDto)
				.collect(Collectors.toList());
		return dtoList;
	}
	
	@GetMapping ("/{id}")
	public DefaultResponse getById(@PathVariable String id) {
		if(repository.findById(id).isPresent() == false) {
			return DefaultResponse.ok("Data Tidak Ditemukan");
		}
		LogEntity entity = repository.findById(id).get();
		LogDto dto = convertToDto(entity);
		return DefaultResponse.ok(dto);
	}
	
	@PostMapping
	public DefaultResponse insert(@RequestBody LogDto dto) {
		if(repository.findById(dto.getIdReq()).isPresent()) {
			return DefaultResponse.ok("Data ID Sudah Terdaftar");
		}
		LogEntity entity = convertToEntity(dto);
		repository.save(entity);
		return DefaultResponse.ok("Data Berhasil Di Input");
	}
	
	@PutMapping ("/update/{id}")
	public DefaultResponse update(@RequestBody LogDto dto, @PathVariable String id) {
		if(repository.findById(id).isPresent() == false) {
			return DefaultResponse.ok("Data Tidak Ditemukan");
		}
		LogEntity entity = convertToEntity(dto);
		entity.setIdReq(id);
		repository.save(entity);
		return DefaultResponse.ok("Data Berhasil di Update");
	}
	
	@DeleteMapping ("/delete/{id}") 
	public DefaultResponse delete(@PathVariable String id) {
		if(repository.findById(id).isPresent() == false) {
			return DefaultResponse.ok("Data Tidak Ditemukan");
		}
		repository.deleteById(id); 
		return DefaultResponse.ok("Data Berhasil di Hapus");
	}
	
	private LogDto convertToDto(LogEntity entity) {
		LogDto dto = new LogDto();
        dto.setIdReq(entity.getIdReq());
        dto.setIdPlatform(entity.getIdPlatform());
        dto.setNamaPlatform(entity.getNamaPlatform());
        dto.setDocType(entity.getDocType());
        dto.setTermOfPayment(entity.getTermOfPayment());
		return dto;
	}
	private LogEntity convertToEntity(LogDto dto) {
        LogEntity entity = new LogEntity();
        entity.setIdReq(dto.getIdReq());
        entity.setIdPlatform(dto.getIdPlatform());
        entity.setNamaPlatform(dto.getNamaPlatform());
        entity.setDocType(dto.getDocType());
        entity.setTermOfPayment(dto.getTermOfPayment());
		return entity;
	}
}
