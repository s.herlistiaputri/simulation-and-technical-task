package com.customid.shinta.model.dto;

import lombok.Data;

@Data
public class ImageDto {
    private String idReq;
    private byte description;
}
