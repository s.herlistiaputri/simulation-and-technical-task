package com.customid.shinta.model.dto;

import lombok.Data;

@Data
public class LogDto {
    private String idReq;
    private String idPlatform;
    private String namaPlatform;
    private String docType;
    private String termOfPayment;
}
