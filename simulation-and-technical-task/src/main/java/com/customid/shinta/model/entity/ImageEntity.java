package com.customid.shinta.model.entity;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table(name = "images_shinta_api01")
public class ImageEntity {
    @Id
    @Column(name = "idrequestbooking")
    private String idReq;
    @Column(name = "description")
    private byte description;
}
