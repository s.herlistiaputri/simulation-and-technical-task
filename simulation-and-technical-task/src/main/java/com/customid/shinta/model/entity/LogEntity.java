package com.customid.shinta.model.entity;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table (name = "log_shinta_api01")
public class LogEntity {
    @Id
    @Column(name = "idrequestbooking")
    private String idReq;
    @Column(name = "id_platform")
    private String idPlatform;
    @Column(name = "nama_platform")
    private String namaPlatform;
    @Column(name = "doc_type")
    private String docType;
    @Column(name = "term_of_payment")
    private String termOfPayment;
}
