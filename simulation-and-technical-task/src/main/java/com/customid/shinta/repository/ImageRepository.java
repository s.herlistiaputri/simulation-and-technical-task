package com.customid.shinta.repository;

import com.customid.shinta.model.entity.ImageEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<ImageEntity, String> {
    
}
