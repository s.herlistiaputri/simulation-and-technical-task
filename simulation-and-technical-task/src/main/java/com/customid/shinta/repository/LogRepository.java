package com.customid.shinta.repository;

import com.customid.shinta.model.entity.LogEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LogRepository extends JpaRepository<LogEntity, String> {
    
}
