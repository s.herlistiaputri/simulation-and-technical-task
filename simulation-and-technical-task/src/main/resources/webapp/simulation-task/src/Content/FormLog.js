import react, {Component} from 'react';
import { Container, Form, FormGroup, Label, Button } from 'react-bootstrap';

export default class FormLog extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }

    render() {
        return (           
            <Container>
                <Form>
                <FormGroup>
                <Label for="IdReq"><b>ID Request Booking</b></Label>
                <Input type="text" name="idReq" id="idReq"
                        value={this.state.idReq} 
                        onChange={this.dataChange}
                        placeholder="ID Request Booking"/>
                </FormGroup>
                <FormGroup>
                <Label for="IdPlatform"><b>ID Platform</b></Label>
                <Input type="text" name="idPlatform" id="idPlatform"
                        value={this.state.idPlatform} 
                        onChange={this.dataChange}
                        placeholder="ID Platform"/>
                </FormGroup>
                <FormGroup>
                <Label for="namaPlatform"><b>Nama Platform</b></Label>
                <Input type="text" name="namaPlatform" id="namaPlatform"
                        value={this.state.namaPlatform} 
                        onChange={this.dataChange}
                        placeholder="Nama Platform"/>
                </FormGroup>                     
                <FormGroup>
                <Label for="docType"><b>Document Type</b></Label>
                <Input type="text" name="docType" id="docType"
                        value={this.state.docType} 
                        onChange={this.dataChange}
                        placeholder="Document Type"/>
                </FormGroup>
                <FormGroup>
                <Label for="TermOfPayments"><b>Term Of Payments</b></Label>
                <Input type="text" name="termOfPayment" id="termOfPayment"
                        value={this.state.termOfPayment} 
                        onChange={this.dataChange}
                        placeholder="Term Of Payments"/>
                </FormGroup>
                <Button type = "submit" id = "submit" className = "btn"></Button>  
                </Form>
           </Container>
            )
    }
    
}